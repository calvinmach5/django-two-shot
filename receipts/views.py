from django.shortcuts import render, redirect
from django.urls import reverse
from receipts.models import Receipt, ExpenseCategory, Account
from django.contrib.auth.decorators import login_required
from receipts.forms import ReceiptForm, ExpenseForm, AccountForm


# Create your views here.
@login_required
def receipt_list_view(request):
    if request.user.is_authenticated:
        receipts = Receipt.objects.filter(purchaser=request.user)
        context = {"receipts": receipts}
        return render(request, "receipts/receipts_list.html", context)
    else:
        receipts = Receipt.objects.none()
    context = {"receipts": receipts}
    return render(request, "receipts/receipts_list.html", context)


@login_required
def categories_list(request):
    expense_categories = ExpenseCategory.objects.filter(owner=request.user)
    context = {"expense_categories": expense_categories}
    return render(request, "receipts/categories_list.html", context)


@login_required
def account_list(request):
    accounts = Account.objects.filter(owner=request.user)
    context = {"accounts": accounts}
    return render(request, "receipts/account_list.html", context)


def redirect_to_receipts_list(request):
    return redirect(reverse("home"))


@login_required
def create_receipt_view(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(commit=False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
    else:
        form = ReceiptForm()
        context = {"form": form}
        return render(request, "receipts/create_receipt.html", context)


@login_required
def create_category_view(request):
    if request.method == "POST":
        form = ExpenseForm(request.POST)
        if form.is_valid():
            expense = form.save(commit=False)
            expense.owner = request.user
            expense.save()
            return redirect("categories_list")
        else:
            print(form.errors)
    else:
        form = ExpenseForm()
        context = {"form": form}
        return render(request, "receipts/create_category.html", context)


@login_required
def create_account_view(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            account = form.save(commit=False)
            account.owner = request.user
            account.save()
            return redirect("account_list")
    else:
        form = AccountForm()
        context = {"form": form}
        return render(request, "receipts/create_account.html", context)
